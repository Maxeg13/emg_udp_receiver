import hypermedia.net.*;        //UDP library

int PORT_RX = 6321;             //Local IP port for listening
String HOST_IP = "127.0.0.1";   //Host IP address for socket - localhost
String REMOTE_IP = "127.0.0.1"; //IP address of host where 8chEmgMonitor is running
int PORT_TX = 6123;             //IP port of 8chEmgMonitor
UDP udp;                        //Create UDP object for recieving
int[] values;                   //Data buffer for graph
int side = 30;                  //Size of buttons
int minusX = 10, minusY = 10;   //Coordinates for minus button
int plusX = minusX + side + 5;  //Coordinates for plus button
int plusY = minusY;     
boolean overPlus, overMinus;    //Flags for cursor hover
byte[] buffer = new byte[2];    //Data buffer for UDP send
byte level = 0;                 //Brightness of the red LED

//Initialization
void setup() {
  udp= new UDP(this, PORT_RX, HOST_IP);  //Create UDP server socket
  udp.listen(true);                      //Start listening for incoming datagram
  size(1280, 480);                       //Size of window
  values = new int[width];               //Init graph buffer
  smooth();                              //Antialiasing
}

//Convert 4 bytes array to int in little endian order
int b2i(byte[] d) {
  return ((d[3] & 0xff) << 24) | ((d[2] & 0xff) << 16) | ((d[1] & 0xff) << 8) | (d[0] & 0xff);
} 

//Receive UDP data event
void receive(byte[] data, String HOST_IP, int PORT_RX) {
  arrayCopy(values, 1, values, 0, width - 1);  //Shift graph buffer
  values[width - 1] = b2i(data) / 50 + 511;    //Add new data to end of the buffer
}

//Compute Y axis value
int getY(int value) {
  return (int)(height - value / 1023.0f * (height - 1));
}

//Paint loop
void draw() {
  background(0);
  stroke(255, 0, 0);
  line(0, height / 2, width, height / 2);   //Draw middle line
  if (overPlus(mouseX, mouseY)) {           //Highlight plus button
    fill(130, 0, 0);
  } else {
    fill(70, 0, 0);
  }
  rect(plusX, plusY, side, side);           //Draw plus button
  fill(255, 0, 0);
  rect(plusX + side / 6, plusY + side / 2 - side / 15, side - side / 3, 2 * side / 15);
  rect(plusX + side / 2 - side / 15, plusY + side / 6, 2 * side / 15, side - side / 3);
  if (overMinus(mouseX, mouseY)) {          //Highlight minus button
    fill(130, 0, 0);
  } else {
    fill(70, 0, 0);
  }
  rect(minusX, minusY, side, side);         //Draw minus button
  fill(255, 0, 0);
  rect(minusX + side / 6, minusY + side / 2 - side / 15, side - side / 3, 2 * side / 15);
  
  stroke(255);
  int y0 = getY(values[0]);
  for (int i = 1; i < width; i++) {
    int y1 = getY(values[i - 1]);
    line(i, y0, i, y1);                     //Draw data line
    y0 = y1;
  }
}

//Check cursor hovers on plus button
boolean overPlus(int x, int y) {
  overPlus = false;
  if (x >= plusX && x <= plusX + side &&  y >= plusY && y <= plusY + side) {
    overPlus = true;
  } 
  return overPlus;
}

//Check cursor hovers on minus button
boolean overMinus(int x, int y) {
  overMinus = false;
  if (x >= minusX && x <= minusX + side &&  y >= minusY && y <= minusY + side) {
    overMinus = true;
  } 
  return overMinus;
}

//Actions on button click
void mousePressed() {
  if (overPlus) {                  //On plus button increase brightness of the red LED
    if (++level > 10) {
      level = 10;
    }
    println("PLUS");
  } else if (overMinus) {
    if (--level < 0) {
      level = 0;
    }
    println("MINUS");
  }
  buffer[0] = 9;                         //Out port number for the red LED
  buffer[1] = level;
  if (udp.send(buffer, REMOTE_IP, PORT_TX))  //Send UDP to remote port
  {
    println("UDP sent");
  }
}