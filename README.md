# EMG_UDP_Receiver
Программа с данной QT-адаптированной библиотекой запускается после запуска программы Хоружко для 8-миканальника ech_monitor.
Также нужно установить Bluegiga Bluetooth Low Energy откуда-то извне

Для компиляции в .pro файле использовать:
QT += network widgets

Пример использования библиотеки

REC=new EMG_UDP_Receiver();
connect(REC,SIGNAL(sig_out(vector<float>)),this,SLOT(getEMG(vector<float>)));